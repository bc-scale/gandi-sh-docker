# Build

Usual invocation:

```shell
time docker build -t bibliosansfrontieres/gandi-sh:php7.4.25 .
```

## Dockerfile explained

### docker-php-entrypoint

The upstream image entrypoint is patched
in order to reconfigure Wordpress to point to the MySQL container.

### prepare-apache.sh

This script installs (at build time) the PHP extensions
found in a regular Gandi Simple Hosting instance.

It was written as a dedicated script in order to avoid a Dockerfile escaping headache.

### wp-su.sh

This wrapper ensures `wp-cli` can run as the `www-data` user
so that permissions remain correct.

### PHP memory_limit

Elementor based blogs may have BIG blobs, wp-cli fails on "memory exhausted".

`wp-su.sh` raises the PHP `memory_limit` to 512Mb
in order to provide this specific configuration tweak to `wp-cli` invocations only.
