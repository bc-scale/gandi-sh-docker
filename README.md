# gandi-sh-docker

Docker Images which tries to replicate the Gandi Simple Hosting environment.

It was designed to be used within the
[`gandi-sh-compose` stack](https://gitlab.com/bibliosansfrontieres/infrastructure/gandi/gandi-sh-compose).

## This image

* enables some Apache modules
* installs/enables some PHP extensions
* generates a (self-signed) TLS certificate
* reconfigures Wordpress to point at the MySQL container

(yeah, it means that this script is hardcoded to be used for Wordpress)

(yeah, it also means that from this step,
your `DOCUMENTROOT_PATH` files are not iso anymore with production)

## Documentation

The full documentation from the [`docs/`](docs/index.md) directory is published at
<https://bibliosansfrontieres.gitlab.io/infrastructure/gandi/gandi-sh-docker>.

## Development

Git repository: <https://gitlab.com/bibliosansfrontieres/infrastructure/gandi/gandi-sh-docker>
