# Usage

This image can be used the same way you would use any `library/php` image:

```shell
docker run -it --rm -v ($pwd):/var/www/html \
                    -p 80:80 -p 443:443 \
                    bibliosansfrontieres/gandi-sh
```

However it was designed to be used within the
[`gandi-sh-compose` stack](https://gitlab.com/bibliosansfrontieres/infrastructure/gandi/gandi-sh-compose).
