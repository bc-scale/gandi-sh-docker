# Contributing

Git repository: <https://gitlab.com/bibliosansfrontieres/infrastructure/gandi/gandi-sh-docker>

## TODO

* ❌ Better UID/GID handling (your `1001` host-side vs `www-data`/`mysql` container-side)
  [doc](https://github.com/docker-library/docs/tree/master/php#running-as-an-arbitrary-user)
* ❌ Add missing PHP extensions
* 💡 Mimic SH paths (`/var/www/html` vs `/srv/data/web/vhosts/$VHOST/htdocs`)
  [doc](https://github.com/docker-library/docs/tree/master/php#changing-documentroot-or-other-apache-configuration)
* 💡 Integrate PHP configuration
  (cf `/opt/php-7.4.25-202110251523/etc/php/fpm/` and the like)
    * ⚠ Consider `/srv/data/etc/php/php-custom.ini`
    * 💡 Script to retrieve the PHP configuration files from a Gandi SH instance
    * [Docker image configuration doc](https://github.com/docker-library/docs/tree/master/php#configuration)
* 💡 Create more images: `gandi-sh:php7.4`, `gandi-sh:php8.1`, etc
* 💡 MySQL images: `gandi-sh:mysql5.7`, etc
    * 💡 Integrate MySQL configuration
    * 💡 Script to retrieve the MySQL configuration files from a Gandi SH instance
* 💡 Make the Wordpress specifics run only if a Wordpress install is detected:
    * `wp-cli` install (and runtime dependencies)
    * `wp-config.php` database host change
* 💡 Docker: remove build dependencies installed by `prepare-apache.sh`
* 💡 move the TLS certificates handling out of `prepare-apache.sh`
