FROM php:7.4.25-apache

COPY prepare-apache.sh /
RUN /prepare-apache.sh


# Patch the entrypoint to tweak WP config to point at the DB container at runtime
RUN sed -i '4i sed -i -e "s/localhost/db/" /var/www/html/wp-config.php' /usr/local/bin/docker-php-entrypoint


# Add sudo in order to run wp-cli as the www-data user 
# hadolint ignore=DL3008
RUN apt-get -qqq update \
    && apt-get install --no-install-recommends -yqqq \
        less \
        sudo \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Add WP-CLI 
COPY wp-su.sh /usr/local/bin/wp
RUN curl --silent https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar \
        -o /usr/local/bin/wp-cli.phar \
    && chmod +x \
        /usr/local/bin/wp-cli.phar \
        /usr/local/bin/wp

CMD ["apache2-foreground"]
