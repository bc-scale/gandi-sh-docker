# gandi-sh-docker

Docker Images which tries to replicate the Gandi Simple Hosting environment.

## This image

* installs `wp-cli`
* enables some Apache modules
* installs/enables some PHP extensions
* generates a (self-signed) TLS certificate
* reconfigures Wordpress to point at the MySQL container

(yeah, it means that this script is hardcoded to be used for Wordpress)

(yeah, it also means that from this step,
your `DOCUMENTROOT_PATH` files are not iso anymore with production)
