#!/bin/bash
# This is a wrapper so that wp-cli can run as the www-data user so that permissions
# remain correct

PHP_MEMORY_LIMIT=512M
#echo "Default PHP memory_limit: $( php -i | fgrep memory_limit )"
#echo "Hardcoded custom PHP memory_limit => ${PHP_MEMORY_LIMIT}"

sudo -E -u www-data php -d memory_limit=${PHP_MEMORY_LIMIT} /usr/local/bin/wp-cli.phar "$@"
