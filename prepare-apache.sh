#!/bin/bash

say() {
    echo >&2 "[+] $*"
}

set -euo pipefail

say "Install dependencies for PHP extensions..."
apt-get update --quiet --quiet \
    && apt-get install --yes --quiet --quiet \
        libbz2-dev \
        libc-client2007e-dev \
        libgmp-dev \
        libicu-dev \
        libldap2-dev \
        libpng-dev \
        libpq-dev \
        libtidy-dev \
        libxml2-dev \
        libxslt-dev \
        libzip-dev
# fuckit - pcntl will complain and I'm too lazy to find the related issue upstream
apt-get install --yes --quiet --quiet \
    freetds-dev \
    && ln -s /usr/lib/x86_64-linux-gnu/libsybdb.so /usr/lib/libsybdb.so

say "Enable PHP extensions..."

# mimic Simple Hosting PHP extensions
#docker-php-ext-install apcu bcmath bz2 calendar dba exif FFI gd gettext gmp imagick imap intl ldap mailparse mongodb pcntl pdo_dblib pdo_mysql pdo_pgsql pgsql shmop soap sockets sysvmsg sysvsem sysvshm tidy xmlrpc xsl yaz Zend OPcache zip Zend OPcache
# apcu FFI imagick mailparse mongodb yaz
for ext in \
    bcmath \
    bz2 \
    calendar \
    dba \
    exif \
    gd \
    gettext \
    gmp \
    intl \
    ldap \
    mysqli \
    opcache \
    pcntl \
    pdo_dblib \
    pdo_mysql \
    pdo_pgsql \
    pgsql \
    shmop \
    soap \
    sockets \
    sysvmsg \
    sysvsem \
    sysvshm \
    tidy \
    xsl \
    zip ; do
        say "Install $ext extension..."
        docker-php-ext-install "$ext"
done

say "Install imap extension..."
docker-php-ext-configure imap --with-kerberos --with-imap-ssl \
    && docker-php-ext-install imap

[[ ! $PHP_VERSION =~ 8\. ]] \
    && say "Install xmlrpc extension (PHP7)..." \
    && docker-php-ext-install \
        xmlrpc

say "Enable Apache modules..."
a2enmod headers ssl rewrite

say "Enable SSL vhost..."
a2ensite default-ssl

say "Generate (self-signed) certificates..."
[ ! -f /etc/ssl/private/ssl-cert-snakeoil.key ] \
    && openssl req -x509 -newkey rsa:2048 \
        -keyout /etc/ssl/private/ssl-cert-snakeoil.key \
        -out /etc/ssl/certs/ssl-cert-snakeoil.pem \
        -days 3560 -nodes -subj "/C=Fr/ST=France/L=Paris/O=bsf/CN=bsf.org"

say "All set."
